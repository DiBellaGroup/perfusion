An important question is which datasets produced these curves. It is the dataset produced by v2.0.0 of the 24 ray notebook. The old-weights (also dubbed sanity check) were produced on a branch that just changed the weights. That branch is gone now but I'll hang onto the perfusion results from it here in case we need them later for whatever reason.

The code for making csvs here explains pretty clearly which is which.

The original source of these files is in

/v/raid1a/dlikhite/collabs/KC/Test_dataset/Processing/Output/
