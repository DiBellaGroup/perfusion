Created 16-Nov-2015 ,  delta_t=0.500000
FLOW_0  1     3.144
FLOW_0  2     2.220
FLOW_0  3     1.838
FLOW_0  4     1.645
FLOW_0  5     1.606
FLOW_0  6     1.977

flowMean and Std    2.07   0.57 , coeff var=   0.28

Scalar_F  1     6.289
Scalar_F  2     3.048
Scalar_F  3     2.562
Scalar_F  4     2.213
Scalar_F  5     2.260
Scalar_F  6     2.614

kwo    1      7.712
kwo    2      6.537
kwo    3      5.257
kwo    4      6.046
kwo    5      5.669
kwo    6      7.465

Tzero    1      0.000
Tzero    2      9.055
Tzero    3     10.640
Tzero    4     10.566
Tzero    5      9.509
Tzero    6      9.095

est_fb   1      0.000
est_fb   2      0.000
est_fb   3      0.000
est_fb   4      0.000
est_fb   5      0.000
est_fb   6      0.000

t0    1      0.800
t0    2      0.800
t0    3      0.800
t0    4      0.800
t0    5      0.800
t0    6      0.800

spillover    1      0.000
spillover    2      0.000
spillover    3      0.000
spillover    4      0.000
spillover    5      0.000
spillover    6      0.000

fval  1      0.705
fval  2      0.250
fval  3      0.267
fval  4      0.305
fval  5      0.149
fval  6      0.149
