Created 16-Nov-2015 ,  delta_t=0.500000
FLOW_0  1     2.666
FLOW_0  2     2.122
FLOW_0  3     1.750
FLOW_0  4     1.623
FLOW_0  5     1.582
FLOW_0  6     1.747

flowMean and Std    1.92   0.41 , coeff var=   0.22

Scalar_F  1     5.332
Scalar_F  2     2.880
Scalar_F  3     2.387
Scalar_F  4     2.171
Scalar_F  5     2.167
Scalar_F  6     2.369

kwo    1      6.534
kwo    2      6.767
kwo    3      5.727
kwo    4      6.178
kwo    5      6.317
kwo    6      6.617

Tzero    1      0.000
Tzero    2      9.131
Tzero    3     10.584
Tzero    4     10.555
Tzero    5      9.453
Tzero    6      9.359

est_fb   1      0.000
est_fb   2      0.000
est_fb   3      0.000
est_fb   4      0.000
est_fb   5      0.000
est_fb   6      0.000

t0    1      0.800
t0    2      0.800
t0    3      0.800
t0    4      0.800
t0    5      0.800
t0    6      0.800

spillover    1      0.000
spillover    2      0.000
spillover    3      0.000
spillover    4      0.000
spillover    5      0.000
spillover    6      0.000

fval  1      0.384
fval  2      0.263
fval  3      0.159
fval  4      0.229
fval  5      0.115
fval  6      0.102
