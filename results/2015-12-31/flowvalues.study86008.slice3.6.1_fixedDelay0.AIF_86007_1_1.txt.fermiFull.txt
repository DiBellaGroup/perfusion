Created 16-Nov-2015 ,  delta_t=0.500000
FLOW_0  1     2.341
FLOW_0  2     2.099
FLOW_0  3     1.999
FLOW_0  4     1.773
FLOW_0  5     2.064
FLOW_0  6     1.740

flowMean and Std    2.00   0.22 , coeff var=   0.11

Scalar_F  1     4.682
Scalar_F  2     2.791
Scalar_F  3     2.644
Scalar_F  4     2.371
Scalar_F  5     2.751
Scalar_F  6     2.421

kwo    1      6.158
kwo    2      7.167
kwo    3      6.514
kwo    4      6.010
kwo    5      6.474
kwo    6      5.897

Tzero    1      0.000
Tzero    2      9.285
Tzero    3     10.426
Tzero    4     10.848
Tzero    5     10.193
Tzero    6      9.554

est_fb   1      0.000
est_fb   2      0.000
est_fb   3      0.000
est_fb   4      0.000
est_fb   5      0.000
est_fb   6      0.000

t0    1      0.346
t0    2      0.346
t0    3      0.346
t0    4      0.346
t0    5      0.346
t0    6      0.346

spillover    1      0.000
spillover    2      0.000
spillover    3      0.000
spillover    4      0.000
spillover    5      0.000
spillover    6      0.000

fval  1      0.330
fval  2      0.204
fval  3      0.192
fval  4      0.283
fval  5      0.143
fval  6      0.081
