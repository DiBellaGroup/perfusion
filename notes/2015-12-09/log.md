Added repository and input results from last analysis.

The first 3 (grid3, grog, nn) are produced using the standing regridding weights. Nothing new here.

The last two compare NUFFT with different sets of weights.

* One set uses the new weights and should therefore give a streakier image but better time curve

* The other uses the old weights and should give a better image with a lesser time curve.


Really this begs the question of "what is better"? Because I'm comparing everything to NUFFT and it is clear from this that NUFFT is not that reliable of a standard.

Well, we have a fairly good idea of a good image, so the question is about the time curves. And since the other 3 are all quite similar there then we have to suspect that NUFFT might be off a little. Especially since it is getting different recon weights and could easily be over / under temporally smoothed.

So we got a different set of temporal weights, got a good image and compared curves. The new one seems to have a more appropriate amount of temporal smoothing judging by the fact that it is very close to the three pre-interpolation methods.
