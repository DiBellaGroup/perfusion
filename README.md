# Perfusion

This folder contains the results of MPI2D work done by Devavrat Likhite on the 24 ray data set using slice 3 and the basal slice.

Since I'm not directly connected to that process I don't have any code to show for it. Where the data came from and where it went is documented in the results folder for each day results were obtained.
